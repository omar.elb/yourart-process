# YourArt POC project

This project serves as a testing result for yourart hiring process

## Assumptions:

This project is the result of the adaptation of the following design: [yourart design](https://storage.googleapis.com/ya-misc/interviews/front/ArtworkPage.png).

Most of data is dynamically rendered except:
 * "Acquire" and "Make an offer" button have no action implemented
 * The delivery estimation, fee and pickup addresses
 * Trial period and reservation information

I added a dynamic entry for country of residence: when specified in the input, the value is updated in the delivery fee statement.

## Setup

before starting the project, make sure to have npm installed. And then once your are on this project directory, run:

### `npm install`

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

