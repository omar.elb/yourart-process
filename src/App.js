import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import NavigationItems from './NavigationItems';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {
            NavigationItems.map((e, i) => (
              <Route path={e.route} element={e.component} key={i}/>
            ))
          }
          <Route
            path="*"
            element={<Navigate to={NavigationItems.find(e => e.isMain).route}/>}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
