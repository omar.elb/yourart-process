import Artwork from "./pages/Artwork";

const NavigationItems = [
    {
        name: "0",
        route: '/artwork/0',
        component: <Artwork url="https://storage.googleapis.com/ya-misc/interviews/front/examples/0.json"/>,
        isMain: true,
    },
    {
        name: "1",
        route: '/artwork/1',
        component: <Artwork url="https://storage.googleapis.com/ya-misc/interviews/front/examples/1.json"/>,
    },
];

export default NavigationItems;