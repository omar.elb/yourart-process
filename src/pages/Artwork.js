import axios from 'axios';
import { useCallback, useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-regular-svg-icons';
import { faChevronRight, faChevronLeft, faTruck, faMapPin, faCheck, faHourglassHalf, faEye, faCube } from '@fortawesome/free-solid-svg-icons';
import _ from 'lodash';

import "./artwork.scss";

function InfoCollapsable(props) {
    const { content, label } = props;
    const [unfolded, setUnfolded] = useState(false);
    return <div className="info-block">
        <div className="label" onClick={() => setUnfolded(!unfolded)}>
            { label }
        </div>
        <div className={`text-info ${unfolded ? 'open' : ''}`}>
            { content }
        </div>
    </div>
}

function Artwork(props) {
    const { url } = props;
    const [artData, setArtData] = useState(null);
    const [deliveryCountry, setDeliveryCountry] = useState('');
    const [deliveryZip, setDeliveryZip] = useState('');
    const [deliverCountryDefered, setDeliverCountryDefered] = useState('_');
    const [carrChevLeft, setCarrChevLeft] = useState(false);
    const [carrChevRight, setCarrChevRight] = useState(true);

    const scrollCarrousel = (toRight) => {
        const carrousel = document.getElementsByClassName("image-scrollable")[0];
        carrousel.scrollLeft += (toRight ? -1 : 1) * carrousel.offsetWidth;
    };

    const setDeferedDeliveryCountry = useCallback(_.throttle(setDeliverCountryDefered, 1500), []);

    const setDeferedDeliveryCountryHandler = (value) => {
        setDeliveryCountry(value);
        setDeferedDeliveryCountry(value);
    };

    const scrollHandler = e => {
        const carrousel = e.target;
        if (carrousel.scrollLeft === 0) {
            setCarrChevLeft(false);
        } else {
            setCarrChevLeft(true);
        }
        if (carrousel.scrollLeft + carrousel.offsetWidth >= carrousel.scrollWidth) {
            setCarrChevRight(false);
        } else {
            setCarrChevRight(true);
        }
    };

    useEffect(() => {
        axios.get(url)
        .then(res => {
            setArtData(res.data);
        })
        .catch(err => {
            console.log('Error fetching json art data', err);
        })
    }, [url])
    return (artData && <div className="artwork">
        <div className="directory-path">
            <div className="unit-path">
                Home <FontAwesomeIcon icon={faChevronRight}/> Painting <FontAwesomeIcon icon={faChevronRight}/> {artData.artistShort.fullname}
            </div>
            <div className="unit-path">
                Artworks <FontAwesomeIcon icon={faChevronRight}/> <div style={{ color: 'black' }}>{artData.title}</div>
            </div>
        </div>
        <div className="information-group">
            <div className="presentation-block">
                <div className="main-photo-block">
                    <img src={artData.imageUrl}></img>
                    <div className="decorations">
                        <div className="unit-block">
                            <FontAwesomeIcon icon={faEye}/>
                            VIEW IN A ROOM
                        </div>
                        <div className="unit-block">
                            <FontAwesomeIcon icon={faCube}/>
                            AR VIEW
                        </div>
                    </div>
                </div>
                <div className="general-info">
                    <InfoCollapsable label="Description" content={artData.description}/>
                    <InfoCollapsable label="Subject, Medium, Style, Materials" content={(<table>
                        <tbody>
                            <tr>
                                <td>Subject:</td>
                                <td>{artData.subjects ? artData.subjects.join(', ') : ''}</td>
                            </tr>
                            <tr>
                                <td>Medium:</td>
                                <td>{artData.mediums ? artData.mediums.join(', ') : ''}</td>
                            </tr>
                            <tr>
                                <td>Style:</td>
                                <td>{artData.styles ? artData.styles.join(', ') : ''}</td>
                            </tr>
                            <tr>
                                <td>Materials:</td>
                                <td>{artData.materials ? artData.materials.join(', ') : ''}</td>
                            </tr>
                        </tbody>
                    </table>)}/>
                </div>
            </div>
            <div className="order-block">
                <div className="unit-block">
                    <div className="order-title">{artData.title}</div>
                    <FontAwesomeIcon icon={faStar}/>
                </div>
                <div className="unit-block">
                    <div className="artist-name">{artData.artistShort.fullname}</div>
                    <div className="artist-country">{artData.artistShort.country || artData.artistShort.countryCode || 'N/A'}</div>
                </div>
                <div className="unit-block">
                    {artData.category}, {artData.creationYear}
                </div>
                <div className="unit-block">
                    {artData.dimensions.width} W x {artData.dimensions.height} H x {artData.dimensions.depth} x D
                </div>
                <div className="price-block">
                    {`${(Math.round(100 * artData.price) / 100).toLocaleString()} €`}
                </div>
                <div className="unit-block extra-space">
                    <button className="btn filled">Acquire</button>
                </div>
                <div className="unit-block extra-space">
                    <button className="btn">Make an offer</button>
                </div>
                <div className="unit-block extra-space left-space">
                    <FontAwesomeIcon icon={faHourglassHalf}/>
                    PRE-RESERVE FOR 24 HOURS
                </div>
                <div className="unit-block left-space">
                    <FontAwesomeIcon icon={faCheck}/>
                    131€ estimated delivery fee for {artData.artistShort.country}
                </div>
                <div className="unit-block extra-space">
                    In order to obtain an accurate delivery fee, please enter your country of residence and zip code:
                </div>
                <div className="unit-block">
                    <input value={deliveryCountry} onChange={e => setDeferedDeliveryCountryHandler(e.target.value)}/>
                    <input value={deliveryZip} onChange={e => setDeliveryZip(e.target.value)}/>
                </div>
                <div className="unit-block left-space">
                    <FontAwesomeIcon icon={faTruck}/>
                    Delivery fee for <div style={{fontWeight: "bolder", margin: '5px'}}>{deliverCountryDefered}</div> is 129€
                </div>
                <div className="unit-block left-space">
                    <FontAwesomeIcon icon={faMapPin}/>
                    Free pickup in <div style={{fontWeight: "bolder", marginLeft: '5px'}}>Brussels, Belgium</div>
                </div>
                <div className="unit-block left-space">
                    <FontAwesomeIcon icon={faCheck}/>
                    Try 14 days at home for free
                </div>
            </div>
        </div>
        <div className="carrousel-group">
            <div className="chev-placerholder">
                {carrChevLeft && <FontAwesomeIcon icon={faChevronLeft} onClick={() => scrollCarrousel(true)}/>}
            </div>
            <div className="image-scrollable" onScroll={scrollHandler}>
                {
                    artData.otherArtworkImages.map(imgSrc => 
                        <img key={Math.floor(Math.random() * 100000).toString()} src={imgSrc}/>
                    )
                }
            </div>
            <div className="chev-placerholder">
                {carrChevRight && <FontAwesomeIcon icon={faChevronRight} onClick={() => scrollCarrousel(false)}/>}
            </div>
        </div>
    </div>)
}

export default Artwork;